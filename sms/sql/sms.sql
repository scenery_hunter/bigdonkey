CREATE DATABASE
IF NOT EXISTS bigdoneky_sms CHARACTER
SET utf8 COLLATE utf8_general_ci;

USE bigdoneky_sms;

DROP TABLE
IF EXISTS `sms_verify_code`;

CREATE TABLE `sms_verify_code` (
	`id` BIGINT (20) NOT NULL AUTO_INCREMENT,
	`send_mobile` VARCHAR (20) NULL DEFAULT NULL COMMENT '手机号',
	`send_message` VARCHAR (255) NULL DEFAULT NULL COMMENT '发送的信息',
	`ip` VARCHAR (20) NULL DEFAULT NULL COMMENT 'ip',
	`valid_until` TIMESTAMP NOT NULL COMMENT '失效时间',
	`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`is_active` TINYINT NOT NULL COMMENT '是否有效 0=否，1=是',
	`msg_type` VARCHAR (20) NULL DEFAULT NULL COMMENT 'register, login, resetpassword, findpassword',
	PRIMARY KEY (`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT = '验证码信息表';

