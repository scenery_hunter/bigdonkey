package bigdonkey.sms.bean;

import lombok.Getter;

/**
 * @author JianHongSun
 */
@Getter
public enum MsgType {

    register("注册"), login("登录"), resetpassword("重置密码"), findpassword("找回密码");

    private String code;

    MsgType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
