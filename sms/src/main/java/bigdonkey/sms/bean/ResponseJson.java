package bigdonkey.sms.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper = true)
public class ResponseJson<T> {

    public enum CodeMsg {
        SUCCESS("OK", "操作成功"),
        FAIL("KO", "操作失败"),
        ERROR("ERR", "操作出错！"),

        OBJECT_NOT_FOUND("ERR0001", "对象不存在"),
        OBJECT_ALREADY_EXISTS("ERR0002", "对象已存在"),
        ERROR_UNKNOWN("ERR9999", "未知错误");

        private String code;
        private String message;
        CodeMsg(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    private String code;

    private String msg;

    private String[] errors;

    private T data;

    public ResponseJson(T data) {
        this.code = CodeMsg.SUCCESS.code;
        this.msg = CodeMsg.SUCCESS.message;
        this.errors = new String[] {};
        this.data = data;
    }

    public ResponseJson(String key, String value) {
        Map map = new HashMap();
        map.put(key, value);
        this.code = CodeMsg.SUCCESS.code;
        this.msg = CodeMsg.SUCCESS.message;
        this.errors = new String[] {};
        this.data = (T) map;
    }

    public ResponseJson(String error, T data) {
        this(CodeMsg.ERROR, error, data);
    }

    public ResponseJson(CodeMsg cm, String[] errors, T data) {
        this.setCode(cm.code);
        this.setMsg(cm.message);
        this.setErrors(errors);
        this.setData(data);
    }
    
    public ResponseJson(CodeMsg cm, T data) {
        this.setCode(cm.code);
        this.setMsg(cm.message);
        this.setErrors(new String[0]);
        this.setData(data);
    }

    public ResponseJson(CodeMsg cm, String error, T data) {
        this(cm, new String[] {error}, data);
    }

    public static <S> ResponseJson<S> error(String[] errors, S data) {
        return new ResponseJson<S>(CodeMsg.ERROR, errors, data);
    }
    
    public static <S> ResponseJson<S> error(String error, S data) {
        return new ResponseJson<S>(CodeMsg.ERROR, new String[]{error}, data);
    }
    
    public static <S> ResponseJson<S> error(CodeMsg cm, S data) {
        return new ResponseJson<S>(cm, data);
    }
    
    public static <S> ResponseJson<S> success(S data) {
        return new ResponseJson<S>(CodeMsg.SUCCESS, new String[0], data);
    }
    
    public static ResponseJson<Object> success(String key, String value) {
        Map map = new HashMap();
        map.put(key, value);
        return new ResponseJson<Object>(CodeMsg.SUCCESS, new String[0], map);
    }

}