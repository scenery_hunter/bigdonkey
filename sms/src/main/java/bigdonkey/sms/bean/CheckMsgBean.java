package bigdonkey.sms.bean;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CheckMsgBean extends MessageBean {

    /**
     * 验证码
     */
    @NotNull
    private String validCode;
}
