package bigdonkey.sms.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author JianHongSun
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendMsgBean {
    private Long msgId;
    private String mobile;
    private String data;
}
