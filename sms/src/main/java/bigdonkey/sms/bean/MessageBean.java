package bigdonkey.sms.bean;


import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author JianHongSun
 */
@Data
public class MessageBean {

    /**
     * 手机号
     */
    @NotNull
    private String mobile;

    /**
     * 验证消息类型
     * "register: 注册, login:登录, resetpassword:重置密码, findpassword:找回密码"
     */
    @NotNull
    private MsgType msgType;
}
