package bigdonkey.sms.repo;

import bigdonkey.sms.bean.MsgType;
import bigdonkey.sms.entity.VerifyCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author JianHongSun
 */
@Repository
public interface VerifyCodeRepository extends CrudRepository<VerifyCode, Long> {
    @Query("select v from VerifyCode v where v.sendMobile = ?1 and v.msgType = ?2 order by v.createTime desc ")
    List<VerifyCode> findByMobile(String mobile, MsgType msgType);
}
