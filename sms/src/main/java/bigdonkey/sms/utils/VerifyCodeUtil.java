package bigdonkey.sms.utils;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public final class VerifyCodeUtil {

    // 验证码随机数取值范围[1~10000)
    private final static int RANDOM_INT_BOUND = 10000;

    // 验证码有效周期（分钟*秒*毫秒15 * 60 * 1000)
    private final static int VALID_UNTIL_PERIOD = 900000;

    /**
     * 生成验证码 - 4位随机数。
     *
     * @return 验证码
     */
    public static String getVerifyCode4Digits() {
        return String.format("%04d", ThreadLocalRandom.current().nextInt(RANDOM_INT_BOUND));
    }

    /**
     * 获得验证码有效期，当前时间+15分钟。
     *
     * @return
     */
    public static Date getVerifyCodeValidUntil() {
        return new Date(System.currentTimeMillis() + VALID_UNTIL_PERIOD);
    }

//    public static void main(String[] args) {
//        long start = System.currentTimeMillis();
//        for (int i = 0; i < 10000; i ++) {
//            System.out.println(getVerifyCode4Digits());
//        }
//        long end = System.currentTimeMillis();
//
//        System.out.printf("%d ms", (end - start));
//    }

}
