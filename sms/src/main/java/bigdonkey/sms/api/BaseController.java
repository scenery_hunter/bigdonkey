package bigdonkey.sms.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author JianHongSun
 */
public class BaseController {

    protected Logger LOG = LoggerFactory.getLogger(this.getClass());

    /**
     * 获取请求IP地址。
     *
     * @return
     */
    protected String getRemoteAddress() {
        return ((ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
    }
}
