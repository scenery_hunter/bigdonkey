package bigdonkey.sms.api;

import bigdonkey.common.exception.Errors;
import bigdonkey.common.exception.SmsException;
import bigdonkey.sms.bean.CheckMsgBean;
import bigdonkey.sms.bean.ResponseJson;
import bigdonkey.sms.entity.VerifyCode;
import bigdonkey.sms.service.MsgService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author JianHongSun
 */
@RestController
@CrossOrigin("*")
public class CheckMsgController {

    @Autowired
    private MsgService msgService;

    @PostMapping("/check_msg")
    public ResponseJson<Boolean> checkMsg(@RequestBody CheckMsgBean checkMsgBean) throws Exception {
        if (null == checkMsgBean.getMobile() || "".equals(checkMsgBean.getMobile())) {
            throw new SmsException(Errors.SMS_PARAM_EXCEPTION);
        } else if (checkMsgBean.getValidCode() == null || checkMsgBean.getValidCode().equals("")) {
            throw new SmsException(Errors.SMS_PARAM_EXCEPTION);
        }

        VerifyCode verifyCode = msgService.findMsgByMobile(checkMsgBean.getMobile(), checkMsgBean.getMsgType());
        if (new Date().after(verifyCode.getValidUntil())) {
            throw new SmsException(Errors.SMS_COCE_EXPIRE_EXCEPRION);
        } else if (!checkMsgBean.getValidCode().equals(verifyCode.getSendMessage())) {
            throw new SmsException(Errors.SMS_COCE_ERROR_EXCEPRION);
        }
        return ResponseJson.success(true);
    }

}
