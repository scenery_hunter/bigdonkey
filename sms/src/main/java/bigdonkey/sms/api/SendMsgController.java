package bigdonkey.sms.api;

import bigdonkey.sms.bean.MessageBean;
import bigdonkey.sms.bean.ResponseJson;
import bigdonkey.sms.bean.SendMsgBean;
import bigdonkey.sms.entity.VerifyCode;
import bigdonkey.sms.service.MsgService;
import bigdonkey.sms.service.impl.SendMsgService;
import bigdonkey.sms.utils.VerifyCodeUtil;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author JianHongSun
 */
@RestController
@CrossOrigin("*")
public class SendMsgController extends BaseController {
    @Autowired
    SendMsgService sendMsgService;

    @Autowired
    MsgService msgService;

    @Value("${sms.message.verifycode.expiredtime}")
    private long expiredtime;

    @RequestMapping(value = "/send_msg", method = RequestMethod.POST,
            consumes = "application/json", produces = "application/json")
    public ResponseJson<SendMsgBean> sendRegisterMessage(@RequestBody MessageBean messageBean) {
        String templeteParam = VerifyCodeUtil.getVerifyCode4Digits();
        try {
            SendSmsResponse smsResponse = sendMsgService.sendMsg(messageBean.getMobile(),
                    messageBean.getMsgType(), templeteParam);
            if (smsResponse.getCode() != null && smsResponse.getCode().equals("OK")) {
                //将验证码存储
                //验证码过期时间
                Date expiredTime = new Date(System.currentTimeMillis() + expiredtime);
                VerifyCode verifyCode = new VerifyCode(messageBean.getMobile(), templeteParam,
                        getRemoteAddress(), expiredTime, true, new Date(), messageBean.getMsgType());
                verifyCode = msgService.saveMsg(verifyCode);
                SendMsgBean bean = new SendMsgBean(verifyCode.getId(), messageBean.getMobile(), templeteParam);
                return ResponseJson.success(bean);
            }
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return ResponseJson.error("发送验证码失败！", new SendMsgBean());
    }
}

