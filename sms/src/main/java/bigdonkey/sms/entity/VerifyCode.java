package bigdonkey.sms.entity;


import bigdonkey.sms.bean.MsgType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author JianHongSun
 */
@Data
@Entity
@Table(name = "sms_verify_code")
@NoArgsConstructor
@AllArgsConstructor
public class VerifyCode {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "send_mobile")
    private String sendMobile;

    /**
     * 发送的信息内容
     */
    @Column(name = "send_message")
    private String sendMessage;

    /**
     * ip地址
     */
    @Column(name = "ip")
    private String ip;

    /**
     * 失效时间
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "valid_until")
    private Date validUntil;

    /**
     * 是否有效 0=否，1=是
     */
    @NotNull
    @Column(name = "is_active", columnDefinition = "tinyint")
    private Boolean isActive;

    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 消息类型
     */
    @Column(name = "msg_type", columnDefinition = "VARCHAR (20)")
    @Enumerated(value = EnumType.STRING)
    private MsgType msgType;

    public VerifyCode(String sendMobile, String sendMessage, String ip, Date validUntil, Boolean isActive, Date createTime, MsgType msgType) {
        this.sendMobile = sendMobile;
        this.sendMessage = sendMessage;
        this.ip = ip;
        this.validUntil = validUntil;
        this.isActive = isActive;
        this.createTime = createTime;
        this.msgType = msgType;
    }
}
