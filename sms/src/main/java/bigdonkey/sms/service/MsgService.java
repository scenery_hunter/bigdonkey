package bigdonkey.sms.service;

import bigdonkey.sms.bean.MsgType;
import bigdonkey.sms.entity.VerifyCode;

/**
 * @author JianHongSun
 */
public interface MsgService {
    VerifyCode saveMsg(VerifyCode code);

    VerifyCode findMsgByMobile(String mobile, MsgType msgType);

}
