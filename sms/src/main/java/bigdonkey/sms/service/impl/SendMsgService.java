package bigdonkey.sms.service.impl;


import bigdonkey.sms.bean.MsgType;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * @author JianHongSun
 */
@Service
public class SendMsgService {

    @Value("${aliyun.message.accessKeyId}")
    public String ACCESS_KEY_ID;

    @Value("${aliyun.message.accessKeySecret}")
    public String ACCESS_KEY_SECRET;

    @Value("${aliyun.message.templateCode.register}")
    public String TEMPLATE_CODE_REGISTER;

    @Value("${aliyun.message.templateCode.login}")
    public String TEMPLATE_CODE_LOGIN;

    @Value("${aliyun.message.templateCode.findPassword}")
    public String TEMPLATE_CODE_FINDPASSWORD;

    @Value("${aliyun.message.templateCode.resetPassword}")
    public String TEMPLATE_CODE_RESETPASSWORD;

    public SendSmsResponse sendMsg(String phoneNumber, MsgType type, String tmplateParam) throws ClientException {
        //设置超时时间-可自行调整
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化ascClient需要的几个参数
        //短信API产品名称（短信产品名固定，无需修改）
        final String product = "Dysmsapi";
        //短信API产品域名（接口地址固定，无需修改）
        final String domain = "dysmsapi.aliyuncs.com";
        String template_code = "";
        if (type.getCode().equals(MsgType.register.getCode())) {
            template_code = TEMPLATE_CODE_REGISTER;
        } else if (type.getCode().equals(MsgType.login.getCode())) {
            template_code = TEMPLATE_CODE_LOGIN;
        } else if (type.getCode().equals(MsgType.resetpassword.getCode())) {
            template_code = TEMPLATE_CODE_RESETPASSWORD;
        } else if (type.getCode().equals(MsgType.findpassword.getCode())) {
            template_code = TEMPLATE_CODE_FINDPASSWORD;
        }
        //初始化ascClient,暂时不支持多region（请勿修改）
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESS_KEY_ID,
                ACCESS_KEY_SECRET);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象
        SendSmsRequest request = new SendSmsRequest();
        //使用post提交
        request.setMethod(MethodType.POST);
        //必填:待发送手机号。
        request.setPhoneNumbers(phoneNumber);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("风光猎人");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(template_code);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        String patam = "{\"code\":\"" + tmplateParam + "\"}";
        request.setTemplateParam(patam);
        request.setOutId(tmplateParam);
        //请求失败这里会抛ClientException异常
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
        return sendSmsResponse;
    }
}
