package bigdonkey.sms.service.impl;

import bigdonkey.common.exception.SmsException;
import bigdonkey.sms.bean.MsgType;
import bigdonkey.sms.entity.VerifyCode;
import bigdonkey.sms.repo.VerifyCodeRepository;
import bigdonkey.sms.service.MsgService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author JianHongSun
 */
@Service
public class MsgServiceImpl implements MsgService {

    @Autowired
    private VerifyCodeRepository verifyCodeRepository;

    /**
     * 保存信息
     */
    @Override
    public VerifyCode saveMsg(VerifyCode code) {
        VerifyCode verifyCode = verifyCodeRepository.save(code);
        return verifyCode;
    }

    @Override
    public VerifyCode findMsgByMobile(String mobile, MsgType msgType) {
        List<VerifyCode> lists = verifyCodeRepository.findByMobile(mobile,msgType);
        if (lists == null || lists.size() == 0) {
            throw new SmsException("验证码不存在");
        }
        return lists.get(0);
    }
}
