package bigdonkey.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * @author JianHongSun
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAuthorizationServer
public class OAuth2CenterServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(OAuth2CenterServiceApplication.class, args);
    }
}
