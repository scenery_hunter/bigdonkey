package bigdonkey.oauth2.repo;

import bigdonkey.oauth2.entity.Hunter;
import org.springframework.data.repository.CrudRepository;

/**
 * @author JianHongSun
 */
public interface HunterRepository extends CrudRepository<Hunter, Long> {

    Hunter findByMobile(String phone);
}
