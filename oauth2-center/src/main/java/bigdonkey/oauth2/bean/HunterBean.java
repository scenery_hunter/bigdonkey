package bigdonkey.oauth2.bean;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class HunterBean {
    @NotNull
    private Long id;

    private String mobile;

    private String password;

    private Integer status;
}
