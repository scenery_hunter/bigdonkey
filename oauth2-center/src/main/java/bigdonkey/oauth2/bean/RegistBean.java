package bigdonkey.oauth2.bean;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author JianHongSun
 */
@Data
public class RegistBean {

    @NotNull
    private String mobile;

    @NotNull
    private String password;

}
