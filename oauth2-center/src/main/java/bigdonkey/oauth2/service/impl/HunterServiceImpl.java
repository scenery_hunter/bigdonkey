package bigdonkey.oauth2.service.impl;


import bigdonkey.oauth2.bean.RegistBean;
import bigdonkey.oauth2.entity.Hunter;
import bigdonkey.oauth2.repo.HunterRepository;
import bigdonkey.oauth2.service.HunterService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author JianHongSun
 */
@Service
@Transactional
public class HunterServiceImpl implements HunterService {

    @Resource
    private HunterRepository hunterRepository;

    @Override
    public Hunter saveHunter(Hunter hunter) {
        return hunterRepository.save(hunter);
    }

    @Override
    public Hunter findByMobile(String phone) {
        return hunterRepository.findByMobile(phone);
    }

    @Override
    public Hunter register(RegistBean bean) {
        Hunter hunter = new Hunter();
        hunter.setMobile(bean.getMobile() == null ? null : bean.getMobile());
        hunter.setRegisterDate(new Date());
        hunter.setStatus(1);
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        hunter.setPassword(bcrypt.encode(bean.getPassword()));
        return hunterRepository.save(hunter);
    }

    @Override
    public Hunter findByUserId(Long userId) {
        return hunterRepository.findOne(userId);
    }

    @Override
    public Hunter updateUser(Hunter hunter) {
        return hunterRepository.save(hunter);
    }

    @Override
    public Hunter findById(Long id) {
        return hunterRepository.findOne(id);
    }
}
