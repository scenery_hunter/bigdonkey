package bigdonkey.oauth2.service;


import bigdonkey.oauth2.entity.Hunter;
import bigdonkey.oauth2.service.impl.HunterServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author JianHongSun
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private HunterServiceImpl hunterService;

    @Override
    public UserDetails loadUserByUsername(String mobile) throws UsernameNotFoundException {
        Hunter hunter = hunterService.findByMobile(mobile);
        if (hunter == null) {
            throw new UsernameNotFoundException("手机号：" + mobile + "未注册！");
        }
        //todo 角色，权限处理
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ADMIN");
        grantedAuthorities.add(grantedAuthority);
        User user = new User(hunter.getMobile(), hunter.getPassword(), grantedAuthorities);
        return user;
    }
}
