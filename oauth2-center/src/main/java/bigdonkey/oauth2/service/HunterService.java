package bigdonkey.oauth2.service;


import bigdonkey.oauth2.bean.RegistBean;
import bigdonkey.oauth2.entity.Hunter;

/**
 * @author JianHongSun
 */
public interface HunterService {
    Hunter saveHunter(Hunter hunter);

    Hunter findByMobile(String phone);

    Hunter register(RegistBean bean);

    Hunter findByUserId(Long userId);

    Hunter updateUser(Hunter hunter);

    Hunter findById(Long id);
}
