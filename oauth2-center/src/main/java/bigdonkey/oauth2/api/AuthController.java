package bigdonkey.oauth2.api;


import bigdonkey.oauth2.bean.HunterBean;
import bigdonkey.oauth2.bean.RegistBean;
import bigdonkey.oauth2.entity.Hunter;
import bigdonkey.oauth2.service.HunterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * @author JianHongSun
 */
@RestController
public class AuthController {

    @Autowired
    private HunterService hunterService;

    @PostMapping(value = "/regist")
    public Hunter register(@RequestBody RegistBean bean) {
        return hunterService.register(bean);
    }

    @GetMapping("/user")
    public Principal user(Principal user){
        return user;
    }


    @PostMapping(value = "/getUser/{userId}")
    public Hunter getUser(@PathVariable("userId") Long userId) {
        Hunter hunter = hunterService.findByUserId(userId);
        return hunter;
    }

    @PostMapping(value = "/update", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE},
            consumes = {MediaType.ALL_VALUE})
    public Hunter updateUser(@RequestBody HunterBean hunterBean) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        Hunter hunter =  hunterService.findById(hunterBean.getId());
        hunter.setId(hunterBean.getId());
        hunter.setMobile(hunterBean.getMobile());
        hunter.setPassword(bcrypt.encode(hunterBean.getPassword()));
        hunter.setStatus(hunterBean.getStatus());
        return hunterService.updateUser(hunter);
    }

}
