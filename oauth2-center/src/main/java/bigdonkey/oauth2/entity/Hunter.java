package bigdonkey.oauth2.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author JianHongSun
 */
@Data
@Entity
@Table(name = "uaa_hunter")
public class Hunter {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "password")
    private String password;

    /**
     * 注册时间
     */
    @Column(name = "register_date")
    private Date registerDate;

    /**
     * 用户状态 1 可用，0 禁用
     */
    //todo 增加冻结状态，提供冻结用户接口，登录时过滤用户状态，之后可用状态的客户才能是实现登录
    @Column(name = "status")
    private Integer status;


}
