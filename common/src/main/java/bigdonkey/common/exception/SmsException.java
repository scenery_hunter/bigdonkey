package bigdonkey.common.exception;

/**
 * @author JianHongSun
 */
public class SmsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public String errorCode;

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public SmsException(String message) {
        super(message);
    }

    public SmsException(String errorCode, String message) {
        super(message);
        this.setErrorCode(errorCode);
    }

    public SmsException(Errors errors) {
        super(errors.getMessage());
        this.setErrorCode(errors.getCode());
    }

}
