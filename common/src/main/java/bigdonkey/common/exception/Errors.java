package bigdonkey.common.exception;

public enum Errors {

    SMS_PARAM_EXCEPTION("DONKEY_SMS_PARAM", "参数异常"),
    SMS_COCE_ERROR_EXCEPRION("DONDKY_SMS_COCE_ERROR", "验证码不正确"),
    SMS_COCE_EXPIRE_EXCEPRION("DONDKY_SMS_COCE_EXPIRE", "验证码已过期"),;

    private String code;
    private String message;

    Errors(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
