# bigdonkey

#### 介绍
基于Spring Cloud微服务化开发平台，可作为后端服务开发脚手架使用，
采用Spring Boot2.0、Spring Cloud、oauth2、Spring data jpa、Mybatis3等技术，具有统一授权、用户管理、网关API管理等多个模块，目前各功能正在逐步完善中。

#### 软件架构
``` java
bigdonkey
├── common -- 统一管理一些基础的jar包
├── eureka -- 服务注册于发现 -- 8182
├── gateway -- zuul网关 -- 8184
├── member -- 会员中心 -- 
├── oauth2-center -- 认证中心 -- 8899
├── sms -- 消息中心 -- 8890
```
#### 使用说明

1. 开发环境

依次启动eureka,gateway后，可启动其他项目，需要将common install到本地，其他项目中有依赖common中的jar.

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

